Problem

Our data sheets have multiple coordinate formats for stations that were sampled back in the 1970s to collect fish. I have input these coordinates into the file Field_Data_Records_Original_Coordinates.csv with their respective station ID (Field Number). These coordinates need to be converted to degree decimal (dd) format with 5 decimal points in a new file so that they can be input into a GPS unit, which will be used when resampling these stations. The reformatted dd coordinates need to be compared to the dd coordinates in a database that used an approximation with 2-4 decimal points for QA/QC. 

I would like to pull the database from it's online source:
https://collections.nmnh.si.edu/search/fishes/
But I don't think this will be possible due to the format of the website's database gui. In that case we will compare it to the file:
SmithsonianCollections_Expedition_SP-78_SP-79.csv
